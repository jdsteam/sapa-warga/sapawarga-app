<?php

namespace app\modules\v1\controllers;

use app\components\UserTrait;
use app\components\WhatsappHelper;
use app\filters\auth\HttpBearerAuth;
use app\models\PasswordChangeForm;
use app\models\PasswordResetForm;
use app\models\PasswordResetRequestForm;
use app\models\PasswordResetTokenVerificationForm;
use app\models\ForgotPasswordForm;
use app\models\ForgotUsernameForm;
use app\models\SignupConfirmForm;
use app\models\SignupForm;
use app\models\UserActivationToken;
use app\models\User;
use app\models\UserOtp;
use app\models\UserChangeProfileForm;
use app\models\UserChangeUsernameForm;
use app\models\UserSearch;
use app\models\AreaSearch;
use app\modules\v1\controllers\Concerns\UserPhotoUpload;
use Illuminate\Support\Arr;
use Yii;
use Carbon\Carbon;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class UserController extends ActiveController
{
    use UserTrait;
    use UserPhotoUpload;

    public $modelClass = 'app\models\User';

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    public function actions()
    {
        return [];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
            ],
        ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => ['get'],
                'view' => ['get'],
                'create' => ['post'],
                'update' => ['put'],
                'update-password' => ['put'],
                'forgot-username' => ['post'],
                'delete' => ['delete'],
                'login' => ['post'],
                'logout' => ['post'],
                'me' => ['get', 'post'],
                'me-photo' => ['get', 'post'],
                'me-change-password' => ['post'],
                'me-change-profile' => ['post'],
                'me-change-username' => ['put'],
                'me-delay-update-username' => ['post']
            ],
        ];

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = [
            'options',
            'login',
            'signup',
            'confirm',
            'update-password',
            'forgot-username',
            'password-reset-request',
            'password-reset-token-verification',
            'password-reset'
        ];

        // setup access
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'update', 'delete'], //only be applied to
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['index', 'view', 'create', 'update', 'delete'],
                    'roles' => ['admin', 'manageUsers'],
                ],
                [
                    'allow' => true,
                    'actions' => ['logout', 'me', 'me-photo', 'me-change-password', 'me-change-profile', 'me-change-username', 'me-delay-update-username'],
                    'roles' => ['user', 'staffRW']
                ]
            ],
        ];

        return $behaviors;
    }

    /**
     * Search users
     *
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionIndex()
    {
        $search = new UserSearch();
        $search->load(Yii::$app->request->get());
        $search->range_roles = [User::ROLE_USER];
        $search->not_in_status = [User::STATUS_DELETED];
        if (!$search->validate()) {
            throw new BadRequestHttpException(
                'Invalid parameters: ' . json_encode($search->getErrors())
            );
        }

        return $search->getDataProvider();
    }

    /**
     * Create new user
     *
     * @return User
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new User();
        $model->scenario = User::SCENARIO_REGISTER;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->validate() && $model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute([$id], true));
        } else {
            // Validation error
            $response = Yii::$app->getResponse();
            $response->setStatusCode(422);

            return $model->getErrors();
        }

        return $model;
    }

    /**
     * Update user
     *
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     * @throws HttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($id)
    {
        $model = $this->actionView($id);

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->validate() && $model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(200);
        } else {
            // Validation error
            $response = Yii::$app->getResponse();
            $response->setStatusCode(422);

            return $model->getErrors();
        }

        return $model;
    }

    /**
     * View user
     *
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $staff = User::find()->where(
            [
                'id' => $id
            ]
        )->andWhere(
            [
                '!=',
                'status',
                -1
            ]
        )->andWhere(
            [
                'role' => User::ROLE_USER
            ]
        )->one();

        if ($staff) {
            return $staff;
        }

        throw new NotFoundHttpException("Object not found: $id");
    }

    /**
     * Delete user
     *
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->actionView($id);

        return $this->applySoftDelete($model);
    }

    /**
     * Process login
     *
     * @return array
     * @throws HttpException
     */
    public function actionLogin()
    {
        $roles = [
            User::ROLE_STAFF_RW,
            User::ROLE_TRAINER,
            User::ROLE_USER,
        ];
        return $this->login($roles);
    }

    public function actionLogout()
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        if ($user) {
            // Remove push notification key
            $user->removePushToken();

            $response = Yii::$app->getResponse();
            $response->setStatusCode(200);
        }
        throw new NotFoundHttpException('Object not found');
    }

    /**
     * Process user sign-up
     *
     * @return string
     * @throws HttpException
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        $model->load(Yii::$app->request->post());
        $response = Yii::$app->getResponse();

        $model->role = $model->role ?: User::ROLE_USER;

        $user = User::findByPhoneAndStatus($model->phone, User::STATUS_PENDING);
        if ($user && UserActivationToken::checkIsActivationTokenExists($user->id)) {
            $response->setStatusCode(422);
            $responseData = ['message' => Yii::t('app', 'error.user.register_again_after_get_Whatsapp')];
            return $responseData;
        }

        if ($model->validate()) {
            $model->signup();
            if (!$this->activationProcess($model)) {
                $response->setStatusCode(422);
                return $model->getErrors();
            }

            $response->setStatusCode(201);
            $responseData = 'true';
            return $responseData;
        }

        $response->setStatusCode(422);
        return $model->getErrors();
    }

    private function activationProcess($model)
    {
        $phone = $model->phone;
        $registeredUser = User::findByPhoneOrUsernameWithAnyStatus($phone);
        $activationToken = new UserActivationToken();
        $activationToken->user_id = $registeredUser->id;

        $token = $activationToken->generateActivationToken(UserActivationToken::SCENARIO_ACTIVATION);
        if (!$token) {
            return false;
        }

        $link = Yii::$app->params['mobileURL'] . '/#/activation-account?token=' . $token;
        $message = Yii::t('app', 'message.activation_user') . $link;
        if (!WhatsappHelper::pushQueue($phone, $message)) {
            return false;
        }

        return true;
    }

    /**
     * Process user sign-up confirmation
     *
     * @return array
     * @throws HttpException
     */
    public function actionConfirm()
    {
        $model = new SignupConfirmForm();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $response = Yii::$app->getResponse();

        if ($model->validate() && $model->confirm()) {
            $user = $model->getUser();
            $response->setStatusCode(200);

            $responseData = [
                'id' => (int)$user->id,
                'access_token' => $user->access_token,
            ];

            return $responseData;
        }
        // Validation error
        $response->setStatusCode(422);

        return $model->getErrors();
    }

    /**
     * Process password reset request
     *
     * @return string
     * @throws HttpException
     */
    public function actionPasswordResetRequest()
    {
        $model = new PasswordResetRequestForm();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $response = Yii::$app->getResponse();

        if ($model->validate() && $model->sendPasswordResetEmail()) {
            $response->setStatusCode(200);

            $responseData = 'true';

            return $responseData;
        }
        // Validation error
        $response->setStatusCode(422);

        return $model->getErrors();
    }

    /**
     * Verify password reset token
     *
     * @return string
     * @throws HttpException
     */
    public function actionPasswordResetTokenVerification()
    {
        $model = new PasswordResetTokenVerificationForm();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $response = Yii::$app->getResponse();

        if ($model->validate() && $model->validate()) {
            $response->setStatusCode(200);
            $responseData = 'true';

            return $responseData;
        }
        // Validation error
        $response->setStatusCode(422);

        return $model->getErrors();
    }

    /**
     * Process password reset
     *
     * @return string
     * @throws HttpException
     */
    public function actionPasswordReset()
    {
        $model = new PasswordResetForm();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $response = Yii::$app->getResponse();

        if ($model->validate() && $model->resetPassword()) {
            $response->setStatusCode(200);
            $responseData = 'true';
            return $responseData;
        }
        $response->setStatusCode(422);
        return $model->getErrors();
    }

    public function actionUpdatePassword()
    {
        $model = new ForgotPasswordForm();
        $model->password = Yii::$app->request->post('password');
        $model->retype_password = Yii::$app->request->post('retype_password');

        $response = \Yii::$app->getResponse();

        $user = $this->getUserByVerifyToken();
        if (!$user) {
            $response->setStatusCode(422);
            $responseData = Yii::t('app', 'error.user.invalid_verify_token');
            return $responseData;
        }

        if (!$model->validate() || !$model->updatePassword($user)) {
            $response->setStatusCode(422);
            return $model->getErrors();
        }

        $response->setStatusCode(204);
        return $response;
    }

    private function getUserByVerifyToken()
    {
        $headers = \Yii::$app->request->headers;
        preg_match('/Bearer\s(\S+)/', $headers['verify-token'], $matches);

        if (!$matches) {
            return false;
        }

        $userID = UserActivationToken::verifyActivationToken($matches[1]);
        if (!$userID) {
            return false;
        }

        $user = User::findIdentity($userID);

        return $user;
    }

    public function actionForgotUsername()
    {
        $model = new ForgotUsernameForm();
        $model->phone = Yii::$app->request->post('phone');
        $model->otp = Yii::$app->request->post('otp');

        $response = \Yii::$app->getResponse();

        $user = User::findByPhoneAndStatus($model->phone, User::STATUS_ACTIVE);
        if (!$user) {
            $response->setStatusCode(422);
            $responseData = Yii::t('app', 'error.user.not_found');
            return $responseData;
        }

        if (!UserOtp::verifyOTP($user->id, $model->otp)) {
            $response->setStatusCode(422);
            $responseData = Yii::t('app', 'error.otp.invalid');
            return $responseData;
        }

        $model->username = $user->username;
        if ($model->validate() && $model->sendWhatsappInfo()) {
            $response->setStatusCode(204);
            return $response;
        }

        $response->setStatusCode(422);
        return $model->getErrors();
    }

    public function actionMeChangePassword()
    {
        $user = User::findIdentity(Yii::$app->user->getId());

        $model = new PasswordChangeForm();
        $model->id = $user->id;
        $model->password_updated_at = $user->password_updated_at;
        $model->password = Yii::$app->request->post('password');
        $model->password_confirmation = Yii::$app->request->post('password_confirmation');
        $model->password_old = Yii::$app->request->post('password_old');
        $response = Yii::$app->getResponse();

        if ($model->validate() && $model->changePassword()) {
            $response->setStatusCode(200);
            $responseData = 'true';
            return $responseData;
        }

        $response->setStatusCode(422);

        return $model->getErrors();
    }

    public function actionMeChangeProfile()
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        $model = new UserChangeProfileForm();
        $model->id = $user->id;
        $model->name = Yii::$app->request->post('name');
        $model->email = Yii::$app->request->post('email');
        $model->phone = Yii::$app->request->post('phone');
        $model->address = Yii::$app->request->post('address');
        $model->job_type_id = Yii::$app->request->post('job_type_id');
        $model->education_level_id = Yii::$app->request->post('education_level_id');
        if (Yii::$app->request->post('file_sk_url')) {
            $model->file_sk_url = Yii::$app->request->post('file_sk_url');
            $model->is_verified = false;
        }
        $response = Yii::$app->getResponse();

        if ($model->validate() && $model->changeProfile()) {
            $response->setStatusCode(200);
            return 'true';
        }

        $response->setStatusCode(422);
        return $model->getErrors();
    }

    public function actionMeDelayUpdateUsername()
    {
        $model = User::findOne(Yii::$app->user->getId());
        $model->is_username_updated = User::USERNAME_NOT_UPDATED;
        $model->username_update_popup_at = date('Y-m-d H:i:s', strtotime('+ 1 week'));
        $response = Yii::$app->getResponse();

        if ($model->save()) {
            $response->setStatusCode(200);
            $responseData = $model;
            return $responseData;
        }

        $response->setStatusCode(422);

        return $model->getErrors();
    }

    public function actionMeChangeUsername()
    {
        $user = User::findIdentity(Yii::$app->user->getId());

        $model = new UserChangeUsernameForm();
        $model->id = $user->id;
        $model->username = Yii::$app->request->post('username');
        $model->phone = Yii::$app->request->post('phone');
        $model->otp = Yii::$app->request->post('otp');
        $model->is_username_updated = User::USERNAME_IS_UPDATED;
        $response = Yii::$app->getResponse();

        if ($model->validate() && $model->verifyOTP() && $model->changeUsername()) {
            $model->sendWhatsappInfo();
            $response->setStatusCode(200);

            $responseData = 'true';
            return $responseData;
        }

        $response->setStatusCode(422);

        return $model->getErrors();
    }

    /**
     * Return logged in user information
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionMe()
    {
        $user = $this->getCurrentUser();
        $user = $this->checkUserIsFirstActivation($user);
        $user = $this->checkUserIsFirstVerification($user);
        return $user;
    }

    private function checkUserIsFirstActivation($user)
    {
        $userToken = UserActivationToken::getUserActivationTokenByID($user['id']);

        if ($userToken) {
            $lastLogin = Carbon::parse($user['last_login_at']);
            $usedAt = Carbon::parse($userToken['used_at']);

            $timeDiff = $usedAt->diffInMinutes($lastLogin);
            $user['is_first_activation'] = ($timeDiff < User::DIFFERENCE_MINUTES);
        }

        return $user;
    }

    private function checkUserIsFirstVerification($user)
    {
        $user['is_first_verified'] = false;
        if ($user['is_verified_at']) {
            $lastLogin = Carbon::parse($user['last_login_at']);
            $verifiedAt = Carbon::parse($user['is_verified_at']);
            $timeDiff = $verifiedAt->diff($lastLogin);

            // TODO format timestamp for is_verified_at and last_login_at is difference
            // one issue is on Dockerfile

            $user['is_first_verified'] = ($timeDiff->h - User::TIMEZONE_HOUR_JAKARTA < User::DIFFERENCE_TIMEZONE_HOUR &&
            $timeDiff->i < User::DIFFERENCE_MINUTES);
        }

        return $user;
    }

    /**
     * Update logged in user information
     *
     * @return array|null|\yii\db\ActiveRecord
     *
     */
    public function actionMeUpdate()
    {
        $allowedAttributes = [
            'username', 'email', 'password', 'name', 'phone', 'address',
            'job_type_id', 'education_level_id', 'birth_date', 'rt', 'rw',
            'lat', 'lon', 'photo_url', 'facebook', 'twitter', 'instagram', 'nik', 'file_sk_url',
        ];

        $attributes = Yii::$app->request->post('UserEditForm');

        return $this->updateCurrentUser(Arr::only($attributes, $allowedAttributes));
    }

    public function actionMePhoto()
    {
       //
    }

    public function actionMePhotoUpload()
    {
        $user = User::findIdentity(Yii::$app->user->getId());

        return $this->processPhotoUpload($user);
    }

    /**
     * Handle OPTIONS
     *
     * @param null $id
     * @return string
     */
    public function actionOptions($id = null)
    {
        return 'ok';
    }
}
