<?php

namespace app\modules\v1\controllers;

use app\models\Otp;
use app\models\OtpDisableForm;
use app\models\OtpRequestForm;
use app\models\OtpVerifyForm;
use app\models\OtpVerifyPasswordForm;
use app\models\UserActivationToken;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use app\filters\auth\HttpBearerAuth;
use app\models\User;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * OTPController implements actions related to OTP generation and OTP verification
 */
class OtpController extends ActiveController
{
    public $modelClass = Otp::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
            ],
            'except' => ['request', 'verify-password'],
        ];

        $behaviors['verbs'] = [
            'class'   => \yii\filters\VerbFilter::className(),
            'actions' => [
                'request' => ['post'],
                'verify'  => ['post'],
                'disable'  => ['post'],
                'request-update' => ['post'],
                'verify-password' => ['post'],
            ],
        ];

        return $this->behaviorAccess($behaviors);
    }

    protected function behaviorAccess($behaviors)
    {
        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];

        $behaviors['authenticator'] = $auth;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only'  => ['verify', 'disable', 'request-update'], //only be applied to
            'rules' => [
                [
                    'allow'   => true,
                    'actions' => ['verify', 'disable', 'request-update'],
                    'roles'   => ['staffRW', 'trainer', 'user'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionRequest()
    {
        $model = new OtpRequestForm();
        $model->phone = Yii::$app->request->post('phone');

        $response = \Yii::$app->getResponse();

        $user = User::findByPhoneAndStatus($model->phone, User::STATUS_ACTIVE);
        if (!$user) {
            $response->setStatusCode(422);
            $responseData = Yii::t('app', 'error.user.not_found');
            return $responseData;
        }

        $model->id = $user->id;
        $model->username = $user->username;

        if ($model->validate() && $model->sendWhatsappInfo()) {
            $response->setStatusCode(200);
            $responseData = 'true';
            return $responseData;
        }

        // Validation error
        $response->setStatusCode(422);
        return $model->getErrors();
    }

    public function actionVerify()
    {
        $user = User::findIdentity(\Yii::$app->user->getId());

        $model = new OtpVerifyForm();
        $model->id = $user->id;
        $model->username = Yii::$app->request->post('username');
        $model->phone = Yii::$app->request->post('phone');
        $model->otp = Yii::$app->request->post('otp');
        $response = \Yii::$app->getResponse();

        if ($model->validate() && $model->verifyOTP()) {
            $response->setStatusCode(204);
            return $response;
        }

        // Validation error
        $response->setStatusCode(422);
        return $model->getErrors();
    }

    public function actionDisable()
    {
        $user = User::findIdentity(\Yii::$app->user->getId());
        $response = \Yii::$app->getResponse();
        $model = new OtpDisableForm();
        $model->id = $user->id;

        if ($model->validate() && $model->findActiveOtp()) {
            $response->setStatusCode(200);
            $responseData = $model->disableOtp();
            return $responseData;
        }

        // Validation error
        $response->setStatusCode(422);
        return $model->getErrors();
    }

    public function actionRequestUpdate()
    {
        $user = User::findIdentity(\Yii::$app->user->getId());

        $response = \Yii::$app->getResponse();

        $model = new OtpRequestForm();
        $model->id = $user->id;
        $model->username = Yii::$app->request->post('username');
        $model->phone = Yii::$app->request->post('phone');

        if ($model->validate() && $model->checkUsername() && $model->sendWhatsappInfo()) {
            $response->setStatusCode(204);
            return $response;
        }

        $response->setStatusCode(422);
        return $model->getErrors();
    }

    public function actionVerifyPassword()
    {
        $model = new OtpVerifyPasswordForm();
        $model->phone = \Yii::$app->request->post('phone');
        $model->otp = \Yii::$app->request->post('otp');
        $response = \Yii::$app->getResponse();

        $user = User::findByPhoneAndStatus($model->phone, User::STATUS_ACTIVE);
        if (!$user) {
            $response->setStatusCode(422);
            $responseData = Yii::t('app', 'error.user.not_found');
            return $responseData;
        }
        $model->id = $user->id;

        if (!$model->validate() || !$model->verifyOTP()) {
            $response->setStatusCode(422);
            return $model->getErrors();
        }

        $tokenModel = new UserActivationToken();
        $tokenModel->user_id = $user->id;
        $token = $tokenModel->generateActivationToken(UserActivationToken::SCENARIO_VERIFICATION);
        if (!$token) {
            $response->setStatusCode(422);
            $responseData = Yii::t('app', 'error.generate.token');
            return $responseData;
        }

        $response->setStatusCode(201);
        return $responseData = [
            'verify_token' => $token,
        ];
    }
}
