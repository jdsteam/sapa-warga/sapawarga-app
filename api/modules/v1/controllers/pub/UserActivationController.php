<?php

namespace app\modules\v1\controllers\pub;

use app\models\UserActivationToken;
use yii\filters\AccessControl;
use app\models\User;
use app\modules\v1\controllers\ActiveController as ActiveController;
use Yii;

/**
 * OTPController implements actions related to OTP generation and OTP verification
 */
class UserActivationController extends ActiveController
{
    public $modelClass = UserActivationToken::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['verbs'] = [
            'class'   => \yii\filters\VerbFilter::className(),
            'actions' => [
                'verify'  => ['get'],
            ],
        ];

        return $this->behaviorCors($behaviors);
    }

    protected function behaviorAccess($behaviors)
    {
        $behaviors['authenticator']['except'] = [
            'verify'
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only'  => ['verify'], //only be applied to
            'rules' => [
                [
                    'allow'   => true,
                    'actions' => ['verify'],
                    'roles'   => ['?'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionVerify()
    {
        $response = \Yii::$app->getResponse();

        $headers = \Yii::$app->request->headers;
        preg_match('/Bearer\s(\S+)/', $headers['activation-token'], $matches);

        if (!$matches) {
            $response->setStatusCode(401);
            $responseData = Yii::t('app', 'error.user.invalid_activation_token');
            return $responseData;
        }

        $userID = UserActivationToken::verifyActivationToken($matches[1]);

        if (!$userID) {
            $response->setStatusCode(401);
            $responseData = Yii::t('app', 'error.user.verify_activation_token');
            return $responseData;
        }

        $record = User::findIdentityWithoutValidation($userID);
        $record->status =  User::STATUS_ACTIVE;
        $record->is_username_updated = 1;
        $record->username_update_popup_at = date('Y-m-d H:i:s');
        $record->save(false);

        $response->setStatusCode(201);
        $responseData = Yii::t('app', 'message.user.activation_success');
        return $responseData;
    }
}
