<?php

namespace app\modules\v1\repositories;

use Yii;
use app\models\User;
use app\models\ParamsCountUser;

class UserRepository
{
    public function getDescendantRoles($roleName)
    {
        $descendantRoles = [];

        if ($roleName === User::ROLE_STAFF_KEL_TAG) {
            $descendantRoles = [User::ROLE_STAFF_RW_TAG];
        }

        if ($roleName === User::ROLE_STAFF_KEC_TAG) {
            $descendantRoles = [User::ROLE_STAFF_RW_TAG, User::ROLE_STAFF_KEL_TAG];
        }

        if ($roleName === User::ROLE_STAFF_KABKOTA_TAG) {
            $descendantRoles = [User::ROLE_STAFF_RW_TAG, User::ROLE_STAFF_KEL_TAG, User::ROLE_STAFF_KEC_TAG];
        }

        if ($roleName === User::ROLE_STAFF_PROV_TAG) {
            $descendantRoles = [
                User::ROLE_STAFF_RW_TAG, User::ROLE_STAFF_KEL_TAG, User::ROLE_STAFF_KEC_TAG,
                User::ROLE_STAFF_KABKOTA_TAG, User::ROLE_TRAINER_TAG, User::ROLE_USER_TAG
            ];
        }

        if ($roleName === User::ROLE_ADMIN_TAG) {
            $descendantRoles = [
                User::ROLE_STAFF_RW_TAG, User::ROLE_STAFF_KEL_TAG, User::ROLE_STAFF_KEC_TAG, User::ROLE_STAFF_KABKOTA_TAG,
                User::ROLE_STAFF_SABERHOAX_TAG, User::ROLE_TRAINER_TAG, User::ROLE_USER_TAG, User::ROLE_STAFF_PROV_TAG
            ];
        }

        return $descendantRoles;
    }

    public function getUsersCountAllRolesByArea($selectedRoles, $kabKotaId, $kecId, $kelId): array
    {
        $roles = Yii::$app->authManager->getRoles();
        $items = [];
        $index = 1;

        foreach ($roles as $role) {
            if (in_array($role->name, $selectedRoles) === false) {
                continue;
            }

            $params = new ParamsCountUser($role->name, $kabKotaId, $kecId, $kelId);
            $params->setStatus(User::STATUS_ACTIVE);

            $item = [
                'id'    => $index,
                'level' => $role->name,
                'name'  => $role->description,
                'value' => $this->getUsersCountRoleByArea($params),
            ];

            if ($role->name === User::ROLE_STAFF_RW_TAG) {
                $params->setNeedCheckVerified(true);
                $item['not_verified'] = $this->getUsersCountRoleByArea($params);
                $params->isVerified = User::USER_IS_VERIFIED;
                $item['verified'] = $this->getUsersCountRoleByArea($params);
                $params->status = User::STATUS_PENDING;
                $params->setNeedCheckVerified(false);
                $item['pending'] = $this->getUsersCountRoleByArea($params);
            }
            $items[] = $item;

            $index++;
        }

        return $items;
    }

    public function getUsersCountRoleByArea(ParamsCountUser $params): int
    {
        $query = User::find()
            ->select('user.id')
            ->innerJoin('auth_assignment', '`auth_assignment`.`user_id` = `user`.`id`')
            ->andWhere(['auth_assignment.item_name' => $params->getRoleName()])
            ->andWhere(['user.status' => $params->getStatus()])
            ->andWhere(['!=', 'user.status', User::STATUS_DELETED])
            ->andWhere(['!=', 'user.role', User::ROLE_SERVICE_ACCOUNT]);

        $isParams = $params->getIsVerified() ? 'is not' : 'is';
        if ($params->getNeedCheckVerified()) {
            $query->andWhere([$isParams, 'user.is_verified', null]);
        }

        if ($params->getKabKotaID()) {
            $query->andWhere(['user.kabkota_id' => $params->getKabKotaID()]);
        }

        if ($params->getKecID()) {
            $query->andWhere(['user.kec_id' => $params->getKecID()]);
        }

        if ($params->getKelID()) {
            $query->andWhere(['user.kel_id' => $params->getKelID()]);
        }

        return $query->count();
    }
}
