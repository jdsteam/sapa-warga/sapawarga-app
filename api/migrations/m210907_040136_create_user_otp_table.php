<?php

use app\components\CustomMigration;

class m210907_040136_create_user_otp_table extends CustomMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_otp', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'otp_hash' => $this->string(200),
            'secret' => $this->string(200),
            'expired_at' => $this->integer(),
            'used_at' => $this->integer()->null(),
            'disabled_at' => $this->integer()->null(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_otp');
    }
}
