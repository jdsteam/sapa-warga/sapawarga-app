<?php

use app\components\CustomMigration;

/**
 * Class m210927_070716_alter_table_user_add_file_sk_url_at */
class m210927_070716_alter_table_user_add_file_sk_url_at extends CustomMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'file_sk_url_uploaded_at', $this->timestamp()->null()->after('file_sk_url'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'file_sk_url_uploaded_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210927_070716_alter_table_user_add_file_sk_url_at cannot be reverted.\n";

        return false;
    }
    */
}
