<?php

use app\components\CustomMigration;

/**
 * Class m211108_102727_alter_table_user_third_account_change_column_account_sifnaker_to_external_account */
class m211108_102727_alter_table_user_third_account_change_column_account_sifnaker_to_external_account extends CustomMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_third_account', 'external_type', $this->string(100)->null());
        $this->renameColumn('user_third_account', 'account_sifnaker', 'external_account_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_third_account', 'external_type');
        $this->renameColumn('user_third_account', 'external_account_id', 'account_sifnaker');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211108_102727_alter_table_user_third_account_change_column_account_sifnaker_to_external_account cannot be reverted.\n";

        return false;
    }
    */
}
