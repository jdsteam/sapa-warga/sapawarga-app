<?php

use app\components\CustomMigration;

/**
 * Class m210915_081639_create_table_user_activation_token */
class m210915_081639_create_table_user_activation_token extends CustomMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user', 'verify_token');
        $this->createTable('user_activation_token', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'token' => $this->string(255),
            'expired_at' => $this->integer(),
            'used_at' => $this->integer()->null(),
            'disabled_at' => $this->integer()->null(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('user', 'verify_token', $this->string(255)->after('password_reset_token'));
        $this->dropTable('user_activation_token');
    }
}
