<?php

use app\components\CustomMigration;

/**
 * Class m210823_024758_alter_table_user_add_nik */
class m210823_024758_alter_table_user_add_nik extends CustomMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'nik', $this->string(20)->after('updated_at'));
        $this->alterColumn('user', 'name', $this->string(200));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'nik');
        $this->alterColumn('user', 'name', $this->string(200)->null());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210823_024758_alter_table_user_add_nik cannot be reverted.\n";

        return false;
    }
    */
}
