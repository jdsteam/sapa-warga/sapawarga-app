<?php

use app\components\CustomMigration;

/**
 * Class m210915_032249_add_is_verified_at_user */
class m210915_032249_add_is_verified_at_user extends CustomMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'is_verified_at', $this->timestamp()->null()->after('is_verified'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'is_verified_at');
    }
}
