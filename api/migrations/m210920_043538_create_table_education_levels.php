<?php

use app\components\CustomMigration;

/**
 * Class m210920_043538_create_table_education_levels */
class m210920_043538_create_table_education_levels extends CustomMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = $this->db->tablePrefix . 'education_levels';
        if ($this->db->getTableSchema($tableName, true) === null) {
            $this->createTable('education_levels', [
                'id'            => $this->primaryKey(),
                'title'         => $this->string()->null(),
                'status'        => $this->tinyinteger(2)->null(),
                'seq'           => $this->tinyinteger(2)->null(),
                'created_at'    => $this->integer()->null(),
                'updated_at'    => $this->integer()->null(),
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('education_levels');
    }
}
