<?php

use app\components\CustomMigration;

/**
 * Class m210823_030418_create_table_user_third_account */
class m210823_030418_create_table_user_third_account extends CustomMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_third_account', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'account_sifnaker' => $this->string(100)->defaultValue(null),
            'created_at' => $this->integer()->defaultValue(null),
            'updated_at' => $this->integer()->defaultValue(null),
            'deleted_at' => $this->integer()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_third_account');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210823_030418_create_table_user_third_account cannot be reverted.\n";

        return false;
    }
    */
}
