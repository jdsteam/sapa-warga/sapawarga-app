<?php

use app\components\CustomMigration;

/**
 * Class m210825_092917_alter_table_user_add_verify_token */
class m210825_092917_alter_table_user_add_verify_token extends CustomMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'verify_token', $this->string(255)->after('password_reset_token'));
        $this->addColumn('user', 'is_verified', $this->boolean()->after('verify_token'));
        $this->addColumn('user', 'file_sk_url', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'verify_token');
        $this->dropColumn('user', 'is_verified');
        $this->dropColumn('user', 'file_sk_url');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210825_092917_alter_table_user_add_verify_token cannot be reverted.\n";

        return false;
    }
    */
}
