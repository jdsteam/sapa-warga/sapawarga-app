<?php

namespace app\models;

use app\components\WhatsappHelper;
use Exception;
use Yii;
use yii\base\Model;

/**
 * User Edit form
 */
class OtpRequestForm extends Model
{
    public $id;
    public $username;
    public $phone;
    public $otp;
    public $message;
    private $_user = false;

    public $minLength = 4;
    public $maxLength = 255;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'string'],
            ['phone', 'required'],
            [
                'phone',
                'match',
                'pattern' => '/^\d{10,15}$/',
                'message' => Yii::t('app', 'error.phone.pattern'),
            ],
            [
                'phone',
                'unique',
                'targetClass' => '\app\models\User',
                'message' => Yii::t('app', 'error.phone.taken'),
                'filter' => function ($query) {
                    $query->andWhere(['!=', 'id', $this->id])->andWhere(['=', 'status', User::STATUS_ACTIVE]);
                }
            ]
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'app.username'),
            'phone' => Yii::t('app', 'app.phone')
        ];
    }

    /**
     * Finds user by [[id]]
     *
     * @return User|null
     */
    public function getUserByID()
    {
        if ($this->_user === false) {
            $this->_user = User::findOne($this->id);
        }

        return $this->_user;
    }

    public function sendWhatsappInfo()
    {
        try {
            if ($this->checkUserOTPActive()) {
                $this->saveOTP();
                $message = \Yii::t('app', 'message.send_otp') . $this->otp . \Yii::t('app', 'message.valid_time') . \Yii::$app->otp->expired . \Yii::t('app', 'app.minute');
                return WhatsappHelper::pushQueue($this->phone, $message);
            }
        } catch (Exception $e) {
            return $this->addError('generic', Yii::t('app', $e->getMessage()));
        }
    }

    public function checkUserOTPActive()
    {
        $checkUserOTPActive = UserOtp::find()
                ->where(['user_id' => $this->id])
                ->andWhere(['used_at' => null])
                ->andWhere(['disabled_at' => null])
                ->andFilterWhere(['>', 'expired_at', time()])
                ->one();
        if (!$checkUserOTPActive) {
            return true;
        }
        return $this->addError('generic', Yii::t('app', 'message.already_request_otp'));
    }

    public function saveOTP()
    {
        $secret = UserOtp::generateSecret([$this->username, $this->phone]);
        $userOtp = new UserOtp();
        $userOtp->user_id = $this->id;
        $this->otp = UserOtp::generateOTP($this->_user, $secret);
        $userOtp->otp_hash = UserOtp::encryptOTP($this->otp, $secret);
        $userOtp->secret = $secret;
        $userOtp->expired_at = strtotime('+' . Yii::$app->otp->expired . ' minutes');
        $userOtp->save();

        if ($userOtp->save(false)) {
            $userOtp->touch('created_at');
            return true;
        }
        return $this->addError('generic', Yii::t('app', 'The system could not update the information.'));
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function checkUsername()
    {
        if ($this->username === null) {
            return $this->addError('generic', Yii::t('app', 'error.username.required'));
        }

        $isLengthEnough = (strlen($this->username) >= $this->minLength && strlen($this->username) < $this->maxLength);
        $pattern = preg_match('/^[a-z0-9_.]/', $this->username);

        if (!$isLengthEnough || !$pattern) {
            return $this->addError('generic', Yii::t('app', 'error.username.pattern'));
        }

        if (User::findOne($this->username)) {
            return $this->addError('generic', Yii::t('app', 'error.username.taken'));
        }

        return true;
    }
}
