<?php

namespace app\models;

use app\components\WhatsappHelper;
use Yii;
use yii\base\Model;

/**
 * User Edit form
 */
class OtpVerifyPasswordForm extends Model
{
    public $id;
    public $phone;
    public $otp;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'otp'], 'required'],
            [['phone', 'otp'], 'trim'],
            ['phone', 'string', 'length' => [10, 13]],
            [
                'phone',
                'match',
                'pattern' => '/^[0-9]{10,13}$/',
                'message' => Yii::t('app', 'error.phone.pattern'),
            ],
            ['otp', 'string', 'length' => [6, 6]],
            [
                'otp',
                'match',
                'pattern' => '/^[0-9]{6,6}$/',
                'message' => Yii::t('app', 'error.otp.pattern')
            ]
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'phone' => Yii::t('app', 'app.phone'),
            'otp' => Yii::t('app', 'app.otp')
        ];
    }

    public function verifyOTP()
    {
        if (UserOtp::verifyOTP($this->id, $this->otp)) {
            return true;
        }
        return $this->addError('generic', Yii::t('app', 'error.otp.invalid'));
    }
}
