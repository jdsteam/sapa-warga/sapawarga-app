<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $password;
    public $name;
    public $phone;
    public $kabkota_id;
    public $kec_id;
    public $kel_id;
    public $rw;
    public $role;
    /** @var User */
    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['name', 'required'],
            ['phone', 'required'],
            ['phone', 'isRegisteredPhoneRule'],
            ['phone',
                'match',
                'pattern' => '/^\d{10,15}$/',
                'message' => Yii::t('app', 'error.phone.pattern')
            ],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [['kabkota_id', 'kec_id', 'kel_id', 'rw'], 'required'],
            ['rw',
                'match',
                'pattern' => '/^\d{3}$/',
                'message' => Yii::t('app', 'error.rw.pattern'),
            ],
            ['role', 'integer'],
        ];

        return array_merge(
            $rules
        );
    }

    protected function rulesEmail()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => '\app\models\User',
                'message' => Yii::t('app', 'This email address has already been taken.')
            ],
        ];
    }

    public function isRegisteredPhoneRule()
    {
        $user = User::find()
            ->where(['=', 'phone', $this->phone])
            ->andWhere(['=', 'status', User::STATUS_ACTIVE])
            ->one();
        if ($user) {
            $this->addError('phone', Yii::t('app', 'error.phone.taken'));
        }
    }

    /**
     * Signs user up.
     *
     * @return boolean the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = $this->findByPhoneWhereStatusNotActive($this->phone);
            if (!$user) {
                $user = new User();
            }
            $user->name = $this->name;
            $user->phone = $this->phone;
            $user->kabkota_id = $this->kabkota_id;
            $user->kec_id = $this->kec_id;
            $user->kel_id = $this->kel_id;
            $user->rw = $this->rw;
            $user->role = $this->role;
            $user->status = User::STATUS_PENDING;
            $user->setPassword($this->password);
            $user->generateAuthKey();

            $user->registration_ip = Yii::$app->request->userIP;

            if ($user->save(false)) {
                $this->_user = $user;
                return true;
            }
        }
        return false;
    }

    /**
     * Return User object
     *
     * @return User
     */
    public function getUser()
    {
        return $this->_user;
    }

    protected function findByPhoneWhereStatusNotActive($phone)
    {
        $user = User::find()
        ->where(['=', 'phone', $phone])
        ->andWhere(['!=', 'status', User::STATUS_ACTIVE])
        ->one();
        return $user;
    }
}
