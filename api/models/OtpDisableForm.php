<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * User Edit form
 */
class OtpDisableForm extends Model
{
    public $id;
    public $otpActive = false;

    public function disableOtp()
    {
        if ($this->otpActive) {
            $this->otpActive->touch('disabled_at');
            $this->otpActive->touch('updated_at');
            return true;
        }
        return $this->addError('generic', Yii::t('app', 'disable failed.'));
    }

    public function findActiveOtp()
    {
        $this->otpActive = UserOtp::find()
                ->where(['user_id' => $this->id])
                ->andWhere(['used_at' => null])
                ->andWhere(['disabled_at' => null])
                ->andFilterWhere(['>', 'expired_at', time()])
                ->one();
        if ($this->otpActive) {
            return true;
        }
        return $this->addError('generic', Yii::t('app', 'No OTP is found'));
    }
}
