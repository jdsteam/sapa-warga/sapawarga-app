<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\NotFoundHttpException;

class UserVerificationCheck extends Model
{
    public $kabkota;
    public $kecamatan;
    public $kelurahan;
    public $rw;
    public $rt;
    public $email;

    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'exist',
                'targetClass' => '\app\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => Yii::t('app', 'There is no user with this email address.')
            ],
            [['kabkota', 'kecamatan', 'kelurahan', 'rw'], 'trim'],
            [['kabkota', 'kecamatan', 'kelurahan', 'rw'], 'required'],
        ];
    }

    public function formName()
    {
        return '';
    }

    public function getDataProvider()
    {
        $kabkota = $this->findAreaId($this->kabkota);
        $kecamatan = $this->findAreaId($this->kecamatan);
        $kelurahan = $this->findAreaId($this->kelurahan);

        $query = User::find()
            ->where(['user.status' => User::STATUS_ACTIVE])
            ->andWhere(['between', 'user.role', User::ROLE_TRAINER, User::ROLE_STAFF_RW])
            ->andWhere(['email' => $this->email])
            ->andWhere(['kabkota_id' => $kabkota])
            ->andWhere(['kec_id' => $kecamatan])
            ->andWhere(['kel_id' => $kelurahan])
            ->andWhere(['rw' => $this->rw])
            ->andWhere(['!=', 'status', User::STATUS_DELETED])
            ->one();

        return $query ? $this->getVerificationCheck($query) : 'Not Found';
    }

    public function getVerificationCheck($query)
    {
        $status = $query->is_verified ? 'Verified' : 'Not Verified';
        return $status;
    }

    public function findAreaId($id)
    {
        $area = Area::find()
            ->select('id')
            ->where(['code_kemendagri' => $id])
            ->one();

        return $area->id ?? null;
    }
}
