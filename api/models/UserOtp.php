<?php

namespace app\models;

use Jdsteam\Sapawarga\Models\Concerns\HasHashedId;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "user_otp".
 *
 * @property int $id
 * @property int $user_id
 * @property string $otp_hash
 * @property string $secret
 * @property int $expired_at
 * @property int $used_at
 * @property int $created_at
 * @property int $updated_at
 */
class UserOtp extends ActiveRecord
{
    use HasHashedId;

    public $hash;
    public $expired;
    public $digit;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_otp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['user_id', 'otp_hash', 'secret', 'created_at'], 'required',
            ],
        ];
    }

    public function fields()
    {
        $fields = [
            'id',
            'user_id',
            'otp_hash',
            'secret',
            'expired_at',
            'used_at',
            'disabled_at',
            'created_at',
            'updated_at',
        ];

        return $fields;
    }

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
        ];
    }

    public static function generateSecret($arrCombination)
    {
        $max = \Yii::$app->otp->digit;
        $secret = bin2hex(random_bytes(random_int(0, $max)));
        foreach ($arrCombination as $combination) {
            $secret .= $combination . uniqid();
        }
        $secret .= time();
        return $secret;
    }

    /**
     * @param string $secret
     * @return string
     */
    public static function generateOTP($data, $secret)
    {
        $result = '';
        for ($i = 0; $i < \Yii::$app->otp->digit; $i++) {
            $result .= random_int(0, 9);
        }

        return $result;
    }

    public static function encryptOTP($otp, $secret)
    {
        return hash_hmac(\Yii::$app->otp->hash, $otp, $secret, false);
    }

    public static function verifyOTP($user_id, $otp)
    {
        $otpActive = UserOtp::find()
                ->where(['user_id' => $user_id])
                ->andWhere(['used_at' => null])
                ->andWhere(['disabled_at' => null])
                ->andFilterWhere(['>', 'expired_at', time()])
                ->one();

        if ($otpActive) {
            $otp_hash = UserOtp::encryptOTP($otp, $otpActive->secret);
            if (hash_equals($otpActive->otp_hash, $otp_hash)) {
                $otpActive->touch('used_at');
                $otpActive->touch('updated_at');
                return true;
            }
        }

        return false;
    }
}
