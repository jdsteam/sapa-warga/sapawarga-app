<?php

namespace app\models;

class ParamsCountUser
{
    public $status;
    public $needCheckVerified = false;
    public $isVerified = null;

    public function __construct($roleName, $kabKotaId, $kecId, $kelId)
    {
        $this->roleName = $roleName;
        $this->kabKotaId = $kabKotaId;
        $this->kecId = $kecId;
        $this->kelId = $kelId;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function setNeedCheckVerified($isNeedCheck)
    {
        $this->needCheckVerified = $isNeedCheck;
    }

    public function getRoleName()
    {
        return $this->roleName;
    }

    public function getKabKotaID()
    {
        return $this->kabKotaId;
    }

    public function getKecID()
    {
        return $this->kecId;
    }

    public function getKelID()
    {
        return $this->kelId;
    }

    public function getIsVerified()
    {
        return $this->isVerified;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getNeedCheckVerified()
    {
        return $this->needCheckVerified;
    }
}
