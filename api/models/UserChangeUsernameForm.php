<?php

namespace app\models;

use app\components\WhatsappHelper;
use Yii;
use yii\base\Model;

/**
 * User Edit form
 */
class UserChangeUsernameForm extends Model
{
    public $id;
    public $username;
    public $phone;
    public $otp;
    public $is_username_updated;
    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'phone', 'otp'], 'required'],
            [['username', 'phone', 'otp'], 'trim'],
            ['username', 'string', 'length' => [4, 255]],
            [
                'username',
                'match',
                'pattern' => '/^[a-z0-9_.]{4,255}$/',
                'message' => Yii::t('app', 'error.username.pattern')
            ],
            [
                'username',
                'unique',
                'targetClass' => '\app\models\User',
                'message' => Yii::t('app', 'error.username.taken'),
                'filter' => function ($query) {
                    $query->andWhere(['!=', 'id', $this->id]);
                }
            ],
            ['phone', 'string', 'length' => [3, 15]],
            [
                'phone',
                'match',
                'pattern' => '/^[0-9]{3,15}$/',
                'message' => Yii::t('app', 'error.phone.pattern'),
            ],
            [
                'phone',
                'unique',
                'targetClass' => '\app\models\User',
                'message' => Yii::t('app', 'error.phone.taken'),
                'filter' => function ($query) {
                    $query->andWhere(['!=', 'id', $this->id]);
                }
            ],
            ['otp', 'string', 'length' => [6, 6]],
            [
                'otp',
                'match',
                'pattern' => '/^[0-9]{6,6}$/',
                'message' => Yii::t('app', 'error.otp.pattern')
            ]
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'app.username'),
            'phone' => Yii::t('app', 'app.phone'),
            'otp' => Yii::t('app', 'app.otp')
        ];
    }

    /**
     * Signs user up.
     *
     * @return boolean the saved model or null if saving fails
     */
    public function changeUsername()
    {
        if ($this->validate()) {
            $this->getUserByID();

            // Set all the other fields
            $attribute_names = $this->attributes();
            foreach ($attribute_names as $name) {
                if ($name != 'otp') {
                    $this->_user[$name] = $this[$name];
                }
            }

            if ($this->_user->save(false)) {
                $this->_user->touch('profile_updated_at');
                $this->_user->touch('username_updated_at');
                return true;
            }
        }
        return $this->addError('generic', Yii::t('app', 'The system could not update the information.'));
    }

    /**
     * Finds user by [[id]]
     *
     * @return User|null
     */
    public function getUserByID()
    {
        if ($this->_user === false) {
            $this->_user = User::findOne($this->id);
        }

        return $this->_user;
    }

    public function sendWhatsappInfo()
    {
        $message = \Yii::t('app', 'message.info_change_username_and_password_success');
        return WhatsappHelper::pushQueue($this->phone, $message);
    }

    public function verifyOTP()
    {
        if (UserOtp::verifyOTP($this->id, $this->otp)) {
            return true;
        }
        return $this->addError('generic', Yii::t('app', 'error.otp.invalid'));
    }
}
