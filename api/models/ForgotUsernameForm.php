<?php

namespace app\models;

use app\components\WhatsappHelper;
use yii\base\Model;
use Yii;

/**
 * ForgotUsernameForm
 */
class ForgotUsernameForm extends Model
{
    public $otp;
    public $phone;
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone','otp'], 'required'],
            [
                'phone',
                'match',
                'pattern' => '/^\d{10,15}$/',
                'message' => Yii::t('app', 'error.phone.pattern')
            ],
            ['otp', 'string', 'length' => [6, 6]],
            [
                'otp',
                'match',
                'pattern' => '/^\d{6,6}$/',
                'message' => Yii::t('app', 'error.otp.pattern')
            ],
        ];
    }

    public function sendWhatsappInfo()
    {
        try {
            $message = \Yii::t('app', 'message.info.forgot_username', [
                'username' => $this->username,
                'phone' => $this->phone,
            ]);
            return WhatsappHelper::pushQueue($this->phone, $message);
        } catch (Exception $e) {
            return $this->addError('generic', Yii::t('app', $e->getMessage()));
        }
    }
}
