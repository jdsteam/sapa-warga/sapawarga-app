<?php

namespace app\models;

use yii\base\Model;
use Yii;

/**
 * ForgotPasswordForm
 */
class ForgotPasswordForm extends Model
{
    public $password;
    public $retype_password;
    /**
     * @var \app\models\User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'retype_password'], 'required'],
            [['password'], 'checkPasswordAndRetypePassword'],
        ];
    }

    public function checkPasswordAndRetypePassword()
    {
        if ($this->password !== $this->retype_password) {
            return $this->addError('password', Yii::t('app', 'error.password.check'));
        }

        if (!$this->checkPassword($this->password) || !$this->checkPassword($this->retype_password)) {
            return $this->addError('password', Yii::t('app', 'error.password.strenghtness'));
        }

        return true;
    }

    public function checkPassword($password)
    {
        // TODO: for now just check if the password has length between 8 until 16 character
        // and alphanumeric
        $isLengthEnough = (strlen($password) >= 8 && strlen($password) < 17)    ;
        $numeric = preg_match('![0-9]+!', $password);
        // $lowerCase = preg_match('![^a-z]+!', $password);
        $alpha = preg_match('![A-Za-z]+!', $password);
        // $hasNonAlpas = preg_match('/\W/', $password);
        $totalTrue = $alpha + $numeric;

        return ($isLengthEnough && $totalTrue === 2);
    }

    public function updatePassword($user)
    {
        if ($this->validate()) {
            $user->setPassword($this->password);
            if ($user->save(false)) {
                return true;
            }
        }

        return false;
    }
}
