<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_activation_token".
 *
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property int $used_at
 * @property int $expired_at
 * @property int $created_at
 * @property int $updated_at
 */

class UserActivationToken extends ActiveRecord
{
    public $activation_expired;
    public $verification_expired;

    // constants for each scenario generate token
    public const SCENARIO_ACTIVATION = 'activation';
    public const SCENARIO_VERIFICATION = 'verification';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_activation_token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['user_id', 'token', 'expired_at'], 'required',
            ],
        ];
    }

    public function fields()
    {
        $fields = [
            'id',
            'user_id',
            'token',
            'expired_at',
            'used_at',
            'disabled_at',
            'created_at',
            'updated_at',
        ];

        return $fields;
    }

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
        ];
    }

    public static function verifyActivationToken($token)
    {
        $tokenActive = static::find()
            ->Where(['used_at' => null])
            ->andWhere(['disabled_at' => null])
            ->andWhere(['token' => $token])
            ->andFilterWhere(['>', 'expired_at', time()])
            ->one();

        if ($tokenActive) {
            $tokenActive->touch('used_at');
            $tokenActive->touch('updated_at');
            $tokenActive->touch('disabled_at');
            return $tokenActive->user_id;
        }

        return false;
    }

    public static function checkIsActivationTokenExists($user_id)
    {
        $token = static::find()
            ->where(['user_id' => $user_id])
            ->andWhere(['used_at' => null])
            ->andWhere(['disabled_at' => null])
            ->andFilterWhere(['>', 'expired_at', time()])
            ->one();
        return ($token !== null);
    }

    public static function getUserIDWithActivationToken($token)
    {
        $userToken = static::find()
            ->where(['token' => $user_id])
            ->andWhere(['used_at' => null])
            ->andFilterWhere(['>', 'expired_at', time()])
            ->one();
        return $userToken->user_id;
    }

    public static function getUserActivationTokenByID($user_id)
    {
        $userToken = static::find()
            ->where(['user_id' => $user_id])
            ->andFilterWhere(['is not', 'used_at', null])
            ->one();
        return $userToken;
    }

    public function generateActivationToken($scenario)
    {
        $this->token = \Yii::$app->security->generateRandomString() . '_' . time();
        if ($scenario === self::SCENARIO_ACTIVATION) {
            $this->expired_at = strtotime('+' . \Yii::$app->token->activation_expired . ' minutes');
        } else {
            $this->expired_at = strtotime('+' . \Yii::$app->token->verification_expired . ' minutes');
        }
        if (!$this->save(false)) {
            return false;
        }

        return $this->token;
    }
}
