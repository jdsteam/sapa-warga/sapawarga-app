<?php

use app\models\UserOtp;

return [
    'class' => UserOtp::class,
    'hash' => getenv('OTP_HASH') ?? 'sha256',
    'expired' => getenv('OTP_EXPIRED') ?? 5,
    'digit' => getenv('OTP_DIGIT') ?? 6,
];
