<?php

use app\models\UserActivationToken;

return [
    'class' => UserActivationToken::class,
    'activation_expired' => getenv('ACTIVATION_TOKEN_TIME') ?? 5,
    'verification_expired' => getenv('VERIFICATION_TOKEN_TIME') ?? 1,
];
